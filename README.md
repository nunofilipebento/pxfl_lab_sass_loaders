SASS LOADERS, JUST CSS WITHOUT JS OR ANIMATED GIFS.  
_____________________________  
Use:
Include _loader.scss.
For load, in the div element with class .holder_loader1, add the class for animation:
	.spinner_sass
	.spinner_xl_sass
	.spinner_01_sass
	.spinner_02_sass
	.spinner_03_sass
	.spinner_rounded

After load, don't left the animation running. 
Example, remove first animation, class .spinner_sass:
	document.querySelectorAll(".holder_loader1")[0].classList.remove("spinner_sass");  
_____________________________  	  